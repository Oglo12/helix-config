#!/usr/bin/env bash

alias hx-up='~/.config/helix/update.sh'

hx-choose-theme () {
  chosen_theme=$(ls ~/.config/helix/themes | grep -v "chosen_theme.toml" | fzf --height=~15% --border=double)

  cp ~/.config/helix/themes/$chosen_theme ~/.config/helix/themes/chosen_theme.toml > /dev/null 2>&1
}
