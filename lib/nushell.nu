alias hx-up = ~/.config/helix/update.sh

def hx-choose-theme [] {
  bash -c "source ~/.config/helix/lib/bash.sh; hx-choose-theme"
}
